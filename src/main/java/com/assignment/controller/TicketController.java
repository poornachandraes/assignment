package com.assignment.controller;

import com.assignment.model.TicketRequest;
import com.assignment.model.TicketResponse;
import com.assignment.service.ITicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TicketController {

    @Autowired
    private ITicketService ticketService;

    @PostMapping("/tickets")
    public TicketResponse createTicket(@RequestBody TicketRequest request) {
        return ticketService.createNewTicket(request);
    }

    @PutMapping("/tickets/{ticketId}")
    public TicketResponse updateTicket(@RequestBody TicketRequest request, @PathVariable Long ticketId) {
        return ticketService.updateTicket(request, ticketId);
    }

    @GetMapping("/tickets")
    public List<TicketResponse> getAllTickets() {
        return ticketService.getAllTickets();
    }

    @GetMapping("/tickets/{ticketId}")
    public TicketResponse getTicketById(@PathVariable("ticketId") Long ticketId) {
        return ticketService.getTicketsById(ticketId);
    }

    @DeleteMapping("/tickets/{ticketId}")
    public boolean deleteTicketById(@PathVariable("ticketId") Long ticketId) {
        return ticketService.deleteTicketsById(ticketId);
    }
}
