package com.assignment.repository;

import com.assignment.model.Ticket;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ITicketRepository {

    public Ticket addTicket(Ticket ticket);
    public Ticket updateTicket(Ticket ticket, Long id);
    public List<Ticket> getAllTicket();
    public Ticket getTicketById(Long id);
    public boolean deleteTicketById(Long id);
}
