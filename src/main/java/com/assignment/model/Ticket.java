package com.assignment.model;

import java.util.Date;

public class Ticket {

    private Long ticketId;
    private String status;
    private Date createdAt;
    private String description;

    public Ticket() {
    }

    Ticket(TicketBuilder builder) {
        this.ticketId = builder.ticketId;
        this.status = builder.status;
        this.createdAt = builder.createdAt;
        this.description = builder.description;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class TicketBuilder {
        private Long ticketId;
        private String status;
        private Date createdAt;
        private String description;

        public static TicketBuilder newInstance() {
            return new TicketBuilder();
        }

        private TicketBuilder() {
        }

        public TicketBuilder withTicketId(Long ticketId) {
            this.ticketId = ticketId;
            return this;
        }

        public TicketBuilder withStatus(String status) {
            this.status = status;
            return this;
        }

        public TicketBuilder withCreatedAt(Date createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public TicketBuilder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
