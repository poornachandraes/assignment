package com.assignment.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.assignment.model.TicketRequest;
import com.assignment.model.TicketResponse;
import com.assignment.model.Ticket;
import com.assignment.repository.ITicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import org.springframework.web.server.ResponseStatusException;

@Service
public class TicketServiceImpl implements ITicketService {

    @Autowired
    private ITicketRepository inMemoryTicketRepository;

    @Override
    public TicketResponse createNewTicket(TicketRequest request) {
        if (request.getDescription() != null && !request.getDescription().isEmpty()) {
            Ticket ticket = Ticket.TicketBuilder.newInstance().withStatus("Open").withDescription(request.getDescription()).build();
            Ticket ticketRes = inMemoryTicketRepository.addTicket(ticket);
            return buildTicketResponse(ticketRes);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Ticket Description is mandatory");
        }
    }

    @Override
    public TicketResponse updateTicket(TicketRequest request, Long ticketId) {
        Ticket ticket = inMemoryTicketRepository.getTicketById(ticketId);
        if (!request.getStatus().isEmpty())
            ticket.setStatus(request.getStatus());
        if (!request.getDescription().isEmpty())
            ticket.setDescription(request.getDescription());
        Ticket ticketRes = inMemoryTicketRepository.updateTicket(ticket, request.getTicketId());
        return buildTicketResponse(ticketRes);
    }

    @Override
    public List<TicketResponse> getAllTickets() {
        List<Ticket> ticketList = inMemoryTicketRepository.getAllTicket();
        List<TicketResponse> ticketResponseList = new ArrayList<>();
        for (Ticket ticket : ticketList) {
            ticketResponseList.add(buildTicketResponse(ticket));
        }
        return ticketResponseList;
    }

    @Override
    public TicketResponse getTicketsById(Long ticketId) {
        Ticket ticketRes = inMemoryTicketRepository.getTicketById(ticketId);
        if (ticketRes != null) {
            return buildTicketResponse(ticketRes);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Ticket not found");
        }
    }

    @Override
    public boolean deleteTicketsById(Long id) {
        return inMemoryTicketRepository.deleteTicketById(id);
    }

    private TicketResponse buildTicketResponse(Ticket ticketRes) {
        TicketResponse response = new TicketResponse();
        response.setTicketId(ticketRes.getTicketId());
        response.setStatus(ticketRes.getStatus());
        response.setCreatedAt(ticketRes.getCreatedAt());
        response.setDescription(ticketRes.getDescription());
        response.setRequestedAt(LocalDateTime.now().toString());
        return response;
    }
}
